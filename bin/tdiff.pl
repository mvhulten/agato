#!/usr/bin/env perl
# Calculate the total amount of hours for the specified account $f.
# Syntax:
#   tdiff.pl ONE-ACCOUNT-FILE
use strict;
use warnings;
use feature qw(say);
use Time::Piece;

my $f = $ARGV[0];

open(FH, '<', $f) or die $!;
my $sum_dt = 0;
while(<FH>){
    my $str_time1;
    my $str_t0 = $1 if ($_ =~ /^([ 0-9][0-9]:[ 0-9][0-9])--/);
    my $str_t1 = $1 if ($_ =~ /--([ 0-9][0-9]:[ 0-9][0-9])[ \t]/);
    my $t0 = Time::Piece->strptime($str_t0, '%H:%M');
    my $t1 = Time::Piece->strptime($str_t1, '%H:%M');
    my $delta = $t1-$t0;
    my $dt = $delta->hours;
    $sum_dt = $sum_dt + $dt;
}
print("$f   $sum_dt\n");

close(FH);
