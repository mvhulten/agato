# Workflow
get-accounts            get unique list of accounts > accounts.txt
gather-account-totals   saves account files in temp/
tdiff.pl                total per account

For instance,

    export period=2022q3
    bin/gather-account-totals -p $period 2022{07,08,09}??.md
    cd $period/
    for f in $(<accounts.txt); do ../bin/tdiff.pl $f >>$period.txt; done
    vim $period.txt
    bc -e $(awk '{ print $2 }' $period.txt | tr "\n" "+" | sed 's/+$//')

The last line is to add the totals.
