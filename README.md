# Time recording & reporting
*Time planning, recording, analysis, reporting and evaluation* are all
important to efficiently structure one's worktime and accounting for
oneself and one's organisation.
Reducing the overhead from the different tools used, each for one or
more of the flow of these activities, would be amendable.
An all-encompassing tool for all of this is probably not feasible.
On the other hand, having small tools for each of those works if they
can be piped, but a more integrated tool would give quicker results.
This tool, *Agato*, does not solve this in full, but does try to address
a void.
The void is that no sane tools exist that integrate even a significant
portion of the chain mentioned above.
The goal of this program is to address planning, recording and reporting.
In addition, you may add task lists and minutes.
Analyses and evaluation should be straightforward but is no priority yet
for Agato.
If you want to use it, refer to the [post-registration
workflow](workflow.md).

For the moment Agato is made to do the following:

1. Make a planning (*journal*) for every new day based on a template
2. The user adjusts the journal throughout the day
3. Calculate the time used per account for a specified period.

*Accounts* may also be called *posts* and can be associated with
projects, tasks and so on.
A colon separates between accounts, subaccounts, subsubaccounts and so
on.
An example of a journal:

```
$ cat 20210914.md
# Hour registration
 8:45--12:00    Metacenter:infra
12:00--14:00    Metacenter:support
14:00--15:15    NIRD2020
15:15--15:55    Learning:Perl
```

It is most practical to associate the main post with that what makes
sense for the economy department (i.e., to report actual money flows).
Subaccounts are more of a task description or a (sub)project belonging
to a main post.

A second paragraph in the same (`/^# Hour.*/`) section may contain some
additional information about hour registration; as of yet none of that
is machine-parsed.
Other sections in this text file may be included (for meeting minutes
and any other notes), as long as they do not start with `# Hour`.
The left column contains the start and end times (which optionally
contain a leading `0` as long as the range symbol `--` is aligned at
column 6).
The right column contains the accounts.
An additional string (task description or comment) between parentheses
(`/ (.*)$/`) is ignored by the machine parsers.
